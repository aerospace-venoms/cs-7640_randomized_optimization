#!/usr/bin/env python3
"""
Gatech OMSCS
CS7641: Machine Learning
Assignment 2: Randomized Optimization
"""

# Includes for skeleton code
import sys
import os
import traceback
import argparse
import time
import logging

# Includes for mlrose-hiive:
import mlrose_hiive as mlrose
import numpy as np

# Includes for graphing results
from sklearn.model_selection import learning_curve
from sklearn.model_selection import ShuffleSplit
import matplotlib.pyplot as plt
import graphviz
from pprint import pprint

# Supervised learning techniques:
"""
from sklearn import tree
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier

"""

# create file handler which logs even debug messages
log = logging.getLogger()
log.setLevel(logging.ERROR)  # DEBUG | INFO | WARNING | ERROR | CRITICAL
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - Line: %(lineno)d\n%(message)s"
)
sh = logging.StreamHandler()
sh.setLevel(logging.ERROR)
sh.setFormatter(formatter)
log.addHandler(sh)
fh = logging.FileHandler(
    os.path.abspath(
        os.path.join(
            os.path.dirname(__file__), os.path.basename(__file__).rstrip(".py") + ".log"
        )
    )
)
fh.setLevel(logging.ERROR)
fh.setFormatter(formatter)
log.addHandler(fh)


def setup_problem(prob, myseed):
    if prob == "k colors":
        description = "K Colors"
        problem = mlrose.MaxKColorGenerator.generate(myseed)
        out_dir = "./data/kcolor/"

    if prob == "4 peaks":
        description = "Four Peaks"
        fitness = mlrose.FourPeaks(t_pct=0.10)
        problem = mlrose.DiscreteOpt(
            length=100, fitness_fn=fitness, maximize=True, max_val=2
        )
        out_dir = "./data/4peaks/"

        output = []
        for i in range(0, 2 ** 10):
            output.append(fitness.evaluate([int(x) for x in "{:08b}".format(i)]))

        plt.plot(
            range(0, 2 ** 10),
            output,
        )
        plt.grid()
        plt.title("The 4 Peaks Problem")
        plt.xlabel("Integer representation of bitstring")
        plt.ylabel("4 Peaks Score")
        plt.savefig("./4peaks.png")

        plt.clf()

    if prob == "knapsack":
        description = "Knapsack"
        problem = mlrose.KnapsackGenerator.generate(seed=myseed, max_item_count=20)

        out_dir = "./data/knapsack/"

    return description, problem, out_dir


def main():
    global args

    # Valid problems:
    # 4 peaks
    # k colors
    # knapsack
    myseed = 42
    description, problem, out_dir = setup_problem("4 peaks", myseed)

    colors = ["red", "orange", "yellow", "green", "blue", "purple"]
    max_iters = 5000

    # Switches for each of the algos:
    do_hill_climb = True
    do_annealing = True
    do_mimic = True
    do_genetic = True

    if do_hill_climb:
        hyperparameter = [1000, 2000, 3000, 4000, 5000]
        hyperparameter_name = "Maximum Attempts"
        algo_name = "Randomized Hill Climbing"
        print("Performing " + algo_name + " on " + description)
        for i in range(len(hyperparameter)):
            algo = mlrose.RHCRunner(
                problem=problem,
                experiment_name=algo_name + " " + description,
                output_directory=out_dir,
                seed=hyperparameter[i],
                iteration_list=2 ** np.arange(10),
                max_attempts=hyperparameter[i],
                restart_list=[0],
                max_iters=max_iters,
            )
            stats, curves = algo.run()

            plt.plot(
                curves["Iteration"],
                curves["Fitness"],
                label=hyperparameter_name + " " + str(hyperparameter[i]),
                color=colors[i],
            )
        plt.grid()
        plt.title(description + " " + algo_name + " Algorithm Fitness vs. Iterations")
        plt.xlabel("Iteration")
        plt.ylabel("Fitness F1")
        plt.legend()
        plt.savefig(out_dir + algo_name + ".png")
        plt.clf()

    if do_annealing:
        hyperparameter = [5, 20, 100, 150, 200]
        hyperparameter_name = "Temperature"
        algo_name = "Simulated Annealing"
        print("Performing " + algo_name + " on " + description)
        for i in range(len(hyperparameter)):
            algo = mlrose.SARunner(
                problem=problem,
                experiment_name=algo_name + " " + description,
                output_directory=out_dir,
                seed=myseed,
                iteration_list=2 ** np.arange(15),
                max_attempts=3000,
                max_iters=max_iters,
                temperature_list=[hyperparameter[i]],
                decay_list=[mlrose.GeomDecay],
            )
            stats, curves = algo.run()

            plt.plot(
                curves["Iteration"],
                curves["Fitness"],
                label=hyperparameter_name + " " + str(hyperparameter[i]),
                color=colors[i],
            )
        plt.grid()
        plt.title(description + " " + algo_name + " Algorithm Fitness vs. Iterations")
        plt.xlabel("Iteration")
        plt.ylabel("Fitness F1")
        plt.legend()
        plt.savefig(out_dir + algo_name + ".png")
        plt.clf()

    if do_mimic:
        hyperparameter = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
        hyperparameter_name = "Keep Rate"
        algo_name = "Mimic"
        print("Performing " + algo_name + " on " + description)

        for i in range(len(hyperparameter)):
            algo = mlrose.MIMICRunner(
                problem=problem,
                experiment_name=algo_name + " " + description,
                output_directory=out_dir,
                seed=myseed,
                iteration_list=2 ** np.arange(10),
                population_sizes=[100],
                max_attempts=10,
                max_iters=max_iters / 10,
                keep_percent_list=[hyperparameter[i]],
                use_fast_mimic=True,
            )
            stats, curves = algo.run()

            plt.plot(
                curves["Iteration"],
                curves["Fitness"],
                label=hyperparameter_name + " " + str(hyperparameter[i]),
                color=colors[i],
            )
        plt.grid()
        plt.title(description + " " + algo_name + " Algorithm Fitness vs. Iterations")
        plt.xlabel("Iteration")
        plt.ylabel("Fitness F1")
        plt.legend()
        plt.savefig(out_dir + algo_name + ".png")
        plt.clf()

    if do_genetic:
        hyperparameter = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
        hyperparameter_name = "Mutation Rate"
        algo_name = "Genetic"
        print("Performing " + algo_name + " on " + description)

        for i in range(len(hyperparameter)):
            algo = mlrose.GARunner(
                problem=problem,
                experiment_name=algo_name + " " + description,
                output_directory=out_dir,
                seed=myseed,
                iteration_list=2 ** np.arange(10),
                max_iters=max_iters / 2,
                population_sizes=[100],
                mutation_rates=[hyperparameter[i]],
            )
            stats, curves = algo.run()
            plt.plot(
                curves["Iteration"],
                curves["Fitness"],
                label=hyperparameter_name + " " + str(hyperparameter[i]),
                color=colors[i],
            )
        plt.grid()
        plt.title(description + " " + algo_name + " Algorithm Fitness vs. Iterations")
        plt.xlabel("Iteration")
        plt.ylabel("Fitness F1")
        plt.legend()
        plt.savefig(out_dir + algo_name + ".png")
        plt.clf()


def get_dataset():

    from sklearn.datasets import fetch_20newsgroups

    # Load the data
    my_dataset = fetch_20newsgroups(
        subset="train",
        data_home="./scikit_learn_data/",
        random_state=0,
        shuffle=True,
        remove=("headers", "footers", "quotes"),
        categories=[
            "rec.autos",
            "sci.crypt",
            "talk.religion.misc",
            "talk.politics.guns",
            "misc.forsale",
        ],
    )

    # Convert the text to vectors to aid in analysis:
    from sklearn.feature_extraction.text import TfidfVectorizer

    vectorizer = TfidfVectorizer()
    vectors = vectorizer.fit_transform(my_dataset.data)

    X = vectors
    y = my_dataset.target
    targetnames = my_dataset.target_names

    return X, y, targetnames


if __name__ == "__main__":

    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(description=__doc__)
        parser.add_argument(
            "-v", "--verbose", action="store_true", default=False, help="verbose output"
        )
        parser.add_argument("-ver", "--version", action="version", version="1.0")
        args = parser.parse_args()
        if args.verbose:
            fh.setLevel(logging.DEBUG)
            log.setLevel(logging.DEBUG)
        log.info("%s Started" % parser.prog)
        main()
        log.info("%s Ended" % parser.prog)
        log.info("Total running time in seconds: %0.2f" % (time.time() - start_time))
        sys.exit(0)
    except KeyboardInterrupt as e:  # Ctrl-C
        raise e
    except SystemExit as e:  # sys.exit()
        raise e
    except Exception as e:
        print("ERROR, UNEXPECTED EXCEPTION")
        print(str(e))
        traceback.print_exc()
        os._exit(1)
