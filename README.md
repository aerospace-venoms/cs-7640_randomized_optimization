# cs7641-supervised_learning

Code for my gatech submission: CS7641 Machine Learning

Running is as simple as:
`./ps1.py -h` for the help.

Valid options for which machine learning techniques to use are:

* dt: Decision tree
* nn: Neural network
* boost: Boosting technique
* svm: Support vector machine
* knn: K-nearest neighbors
