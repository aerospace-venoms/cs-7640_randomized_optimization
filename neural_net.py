#!/usr/bin/env python3
"""
Gatech OMSCS
CS7641: Machine Learning
Assignment 2: Randomized Optimization
"""

# Includes for skeleton code
import sys
import os
import traceback
import argparse
import time
import logging

# Includes for mlrose-hiive:
import mlrose_hiive as mlrose
import numpy as np

# Includes for graphing results
from sklearn.model_selection import learning_curve
from sklearn.model_selection import ShuffleSplit
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import scale
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
import graphviz
from pprint import pprint

# Supervised learning techniquess
from sklearn.neural_network import MLPClassifier

# create file handler which logs even debug messages
log = logging.getLogger()
log.setLevel(logging.ERROR)  # DEBUG | INFO | WARNING | ERROR | CRITICAL
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - Line: %(lineno)d\n%(message)s"
)
sh = logging.StreamHandler()
sh.setLevel(logging.ERROR)
sh.setFormatter(formatter)
log.addHandler(sh)
fh = logging.FileHandler(
    os.path.abspath(
        os.path.join(
            os.path.dirname(__file__), os.path.basename(__file__).rstrip(".py") + ".log"
        )
    )
)
fh.setLevel(logging.ERROR)
fh.setFormatter(formatter)
log.addHandler(fh)


def setup_problem(prob, myseed):
    if prob == "k colors":
        description = "K Colors"
        problem = mlrose.MaxKColorGenerator.generate(myseed)
        out_dir = "./data/kcolor/"

    if prob == "4 peaks":
        description = "Four Peaks"
        fitness = mlrose.FourPeaks(t_pct=0.10)
        problem = mlrose.DiscreteOpt(
            length=100, fitness_fn=fitness, maximize=True, max_val=2
        )
        out_dir = "./data/4peaks/"

    if prob == "knapsack":
        description = "Knapsack"
        problem = mlrose.KnapsackGenerator.generate(seed=myseed)

        out_dir = "./data/knapsack/"

    return description, problem, out_dir


def main():
    global args

    # Valid problems:
    # 4 peaks
    # k colors
    # knapsack
    myseed = 42
    colors = ["red", "orange", "yellow", "green", "blue", "purple"]
    max_iters = 100
    out_dir = "./data/nn"
    description = "Neural Network 20 Newsgroups"

    # Get the training data:
    X, y, target_names = get_dataset()

    # Switches for each of the algos:
    do_original = False
    do_hill_climb = True
    do_annealing = True
    do_mimic = True
    do_genetic = True

    if do_original:
        description = "Neural Network 20 Newsgroups, ADAM with backpropogation"
        print("Performing original neural net data")
        # Original neural net case:
        algo_name = "ADAM"
        clf = MLPClassifier(
            solver="adam",
            alpha=0.0001,
            hidden_layer_sizes=(2, 2),
            random_state=42,
            max_iter=200,
            early_stopping=True,
            learning_rate="constant",
        )

        do_learning(
            clf=clf,
            X=X,
            y=y,
            target_names=target_names,
            algo_name=algo_name,
            title=description,
        )

    if do_hill_climb:
        description = "Neural Network 20 Newsgroups, Randomized Hill Climbing"
        print("Performing hill climbing NN")
        algo_name = "Hill Climb"
        # NN Hill Climbing case:
        clf = mlrose.NeuralNetwork(
            hidden_nodes=[2, 2],
            algorithm="random_hill_climb",
            max_iters=200,
            learning_rate=0.0001,
            is_classifier=True,
            random_state=myseed,
            restarts=0,
        )

        X, y = shuffle(X, y, random_state=42)

        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.2, random_state=42
        )
        print("NP shape:", np.shape(X), np.shape(y))
        clf.fit(X=X_train, y=y_train)
        y_train_pred = clf.predict(X_train)

        """
        cv = ShuffleSplit(n_splits=100, test_size=0.2, random_state=42)

        for train_index, test_index in cv.split(y):
            # Training
            A = X[train_index]
            B = y[train_index]
            clf.fit(X[train_index], y[train_index])
            # Cross-validation
            # score = clf.score(X[test_index], y[test_index])
            # scores.append(score)
            samples.append(len(train_index) + len(test_index))

        print("Samples:", samples)
        print("Scores:", scores)
        """


def get_dataset():

    from sklearn.datasets import fetch_20newsgroups

    # Load the data
    my_dataset = fetch_20newsgroups(
        subset="train",
        data_home="./scikit_learn_data/",
        random_state=0,
        shuffle=True,
        remove=("headers", "footers", "quotes"),
        categories=[
            "rec.autos",
            "sci.crypt",
            "talk.religion.misc",
            "talk.politics.guns",
            "misc.forsale",
        ],
    )

    # Convert the text to vectors to aid in analysis:
    from sklearn.feature_extraction.text import TfidfVectorizer

    vectorizer = TfidfVectorizer()
    vectors = vectorizer.fit_transform(my_dataset.data)

    X = vectors
    y = my_dataset.target
    targetnames = my_dataset.target_names

    return X, y, targetnames


def do_learning(clf, X, y, target_names, title, algo_name):

    # Shuffle the data and get a learning curve for the data
    cv = ShuffleSplit(n_splits=100, test_size=0.2, random_state=42)
    plot_learning_curve(
        clf,
        title=title,
        X=X[0:200],
        y=y[0:200],
        cv=cv,
        algo_name=algo_name,
    )


def plot_learning_curve(
    estimator,
    title,
    X,
    y,
    algo_name,
    axes=None,
    ylim=None,
    cv=None,
    n_jobs=None,
    train_sizes=np.linspace(0.1, 1.0, 5),
):
    # I found this helpful code at: https://scikit-learn.org/stable/auto_examples/model_selection/plot_learning_curve.html
    """
    Generate 3 plots: the test and training learning curve, the training
    samples vs fit times curve, the fit times vs score curve.

    Parameters
    ----------
    estimator : estimator instance
        An estimator instance implementing `fit` and `predict` methods which
        will be cloned for each validation.

    title : str
        Title for the chart.

    X : array-like of shape (n_samples, n_features)
        Training vector, where ``n_samples`` is the number of samples and
        ``n_features`` is the number of features.

    y : array-like of shape (n_samples) or (n_samples, n_features)
        Target relative to ``X`` for classification or regression;
        None for unsupervised learning.

    axes : array-like of shape (3,), default=None
        Axes to use for plotting the curves.

    ylim : tuple of shape (2,), default=None
        Defines minimum and maximum y-values plotted, e.g. (ymin, ymax).

    cv : int, cross-validation generator or an iterable, default=None
        Determines the cross-validation splitting strategy.
        Possible inputs for cv are:

          - None, to use the default 5-fold cross-validation,
          - integer, to specify the number of folds.
          - :term:`CV splitter`,
          - An iterable yielding (train, test) splits as arrays of indices.

        For integer/None inputs, if ``y`` is binary or multiclass,
        :class:`StratifiedKFold` used. If the estimator is not a classifier
        or if ``y`` is neither binary nor multiclass, :class:`KFold` is used.

        Refer :ref:`User Guide <cross_validation>` for the various
        cross-validators that can be used here.

    n_jobs : int or None, default=None
        Number of jobs to run in parallel.
        ``None`` means 1 unless in a :obj:`joblib.parallel_backend` context.
        ``-1`` means using all processors. See :term:`Glossary <n_jobs>`
        for more details.

    train_sizes : array-like of shape (n_ticks,)
        Relative or absolute numbers of training examples that will be used to
        generate the learning curve. If the ``dtype`` is float, it is regarded
        as a fraction of the maximum size of the training set (that is
        determined by the selected validation method), i.e. it has to be within
        (0, 1]. Otherwise it is interpreted as absolute sizes of the training
        sets. Note that for classification the number of samples usually have
        to be big enough to contain at least one sample from each class.
        (default: np.linspace(0.1, 1.0, 5))
    """
    """
    if axes is None:
        _, axes = plt.subplots(1, 3, figsize=(20, 5))
    """

    if ylim is not None:
        plt.set_ylim(*ylim)
    plt.xlabel("Training examples")
    plt.ylabel("Score")

    train_sizes, train_scores, test_scores, fit_times, _ = learning_curve(
        estimator,
        X,
        y,
        cv=cv,
        n_jobs=n_jobs,
        train_sizes=train_sizes,
        return_times=True,
    )
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    fit_times_mean = np.mean(fit_times, axis=1)
    fit_times_std = np.std(fit_times, axis=1)

    # Plot learning curve
    plt.grid()
    plt.fill_between(
        train_sizes,
        train_scores_mean - train_scores_std,
        train_scores_mean + train_scores_std,
        alpha=0.1,
        color="r",
    )
    plt.fill_between(
        train_sizes,
        test_scores_mean - test_scores_std,
        test_scores_mean + test_scores_std,
        alpha=0.1,
        color="g",
    )
    plt.plot(train_sizes, train_scores_mean, "o-", color="r", label="Training score")
    plt.plot(
        train_sizes, test_scores_mean, "o-", color="g", label="Cross-validation score"
    )
    plt.legend(loc="best")

    """
    # Plot n_samples vs fit_times
    axes[1].grid()
    axes[1].plot(train_sizes, fit_times_mean, "o-")
    axes[1].fill_between(
        train_sizes,
        fit_times_mean - fit_times_std,
        fit_times_mean + fit_times_std,
        alpha=0.1,
    )
    axes[1].set_xlabel("Training examples")
    axes[1].set_ylabel("fit_times")
    axes[1].set_title("Scalability of the model")

    # Plot fit_time vs score
    axes[2].grid()
    axes[2].plot(fit_times_mean, test_scores_mean, "o-")
    axes[2].fill_between(
        fit_times_mean,
        test_scores_mean - test_scores_std,
        test_scores_mean + test_scores_std,
        alpha=0.1,
    )
    axes[2].set_xlabel("fit_times")
    axes[2].set_ylabel("Score")
    axes[2].set_title("Performance of the model")
    """

    plt.title(title)
    plt.legend()
    plt.savefig("./data/nn/" + algo_name + ".png")
    plt.clf()
    return


if __name__ == "__main__":

    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(description=__doc__)
        parser.add_argument(
            "-v", "--verbose", action="store_true", default=False, help="verbose output"
        )
        parser.add_argument("-ver", "--version", action="version", version="1.0")
        args = parser.parse_args()
        if args.verbose:
            fh.setLevel(logging.DEBUG)
            log.setLevel(logging.DEBUG)
        log.info("%s Started" % parser.prog)
        main()
        log.info("%s Ended" % parser.prog)
        log.info("Total running time in seconds: %0.2f" % (time.time() - start_time))
        sys.exit(0)

    except KeyboardInterrupt as e:  # Ctrl-C
        raise e
    except SystemExit as e:  # sys.exit()
        raise e
    """
    except Exception as e:
        print("ERROR, UNEXPECTED EXCEPTION")
        print(str(e))
        traceback.print_exc()
        os._exit(1)
    """
